﻿using System;
using System.Globalization;
using System.Linq;

namespace Holomek_camelCase
{
    class Program
    {
        static void Main(string[] args)
        {
            start:
            Console.Write("Prosím, zadejte text ke konverzi: ");
            string text = Console.ReadLine();
            TextInfo a = CultureInfo.InvariantCulture.TextInfo;
            text = a.ToTitleCase(text).Replace(" ", string.Empty);
            konverze:
            Console.WriteLine("\nLower-camelCase (1)\nUpper-CamelCase (2)\nOpakovat zadání (3)");
            int metoda = int.Parse(Console.ReadLine());
            if (metoda == 1)
            {
                text = $"{text.First().ToString().ToLowerInvariant()}" + $"{text.Substring(1)}";
                Console.WriteLine("");
                Console.WriteLine("Výsledek: {0}", text);
                Console.WriteLine("______________________\nmetoda Lower-camelCase\n");
            }
            else if (metoda == 2)
            {
                text = $"{text.First().ToString().ToUpperInvariant()}" + $"{text.Substring(1)}";
                Console.WriteLine("");
                Console.WriteLine("Výsledek: {0}", text);
                Console.WriteLine("______________________\nmetoda Upper-CamelCase\n");
            }
            else if (metoda == 3)
                goto start;
            else
                Console.WriteLine("\nNavigace klávesami: 1, 2, 3");
            goto konverze;
        }
    }
}
